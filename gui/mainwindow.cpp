#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QWidget(parent)
{
    setWindowTitle("мПАК-ПП");
    setLayout(new QGridLayout);
    _teAgilentE364XA = new QTextEdit();

    auto tabWidget = new QTabWidget;

    QWidget* tabAgilentE364XA = new QWidget();
    {
        auto tabLayout = new QGridLayout();

        auto gbInput = new QGroupBox("Ввод данных");
        {
            auto gbInputLayout = new QGridLayout();

            QStringList commands = {
                "bool open()",
                "bool close()",
                "bool isEmulation()",
                "bool setEmulation(bool value)",
                "bool isOpen()",
                "int getId()",
                "void setDeviceType(AgilentDeviceType type)",
                "AgilentDeviceType getDeviceType()",
                "bool setVoltage(qreal volt, quint8 out)",
                "bool setCurrent(qreal curr, quint8 out)",
                "bool setOutput(quint8 out)",
                "bool setOutputEnabled(bool enable)",
                "bool clear()",
                "qreal getVoltage(quint8 out)",
                "qreal getCurrent(quint8 out)",
                "quint8 getOutput()",
                "bool getOutputEnabled()",
                "void setNameDevice(const QString &nameDev)",
                "QString getNameDevice()"
            };

            auto btmConnect = new QPushButton("Подключиться");
            auto btmDisconnect = new QPushButton("Отключиться");
            auto cbCommandList = new QComboBox();
            cbCommandList->addItems(commands);
            auto btmSendCommand = new QPushButton("Отправить команду");

            gbInputLayout->addWidget(btmConnect, 0, 0);
            gbInputLayout->addWidget(btmDisconnect, 0, 1);
            gbInputLayout->addWidget(new QLabel("Список команд"), 1, 0);
            gbInputLayout->addWidget(cbCommandList, 1, 1, 1, 2);
            gbInputLayout->addWidget(btmSendCommand, 1, 3, 1, 1);

            gbInput->setLayout(gbInputLayout);
        }

        auto gbOutput = new QGroupBox("Вывод данных");
        {
            auto gbOutputLayout = new QGridLayout();
            log("Приложение запущено");

            gbOutputLayout->addWidget(_teAgilentE364XA, 0, 0);
            gbOutput->setLayout(gbOutputLayout);
        }

        tabLayout->addWidget(gbInput, 0, 0);
        tabLayout->addWidget(gbOutput, 1, 0);
        tabAgilentE364XA->setLayout(tabLayout);
    }

    tabWidget->addTab(tabAgilentE364XA, "AgilentE364XA");

    layout()->setMargin(0);
    layout()->addWidget(tabWidget);
}

void MainWindow::log(const QString& message)
{
    _teAgilentE364XA->append(QString("%1 %2")
                             .arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz"))
                             .arg(message));
}
